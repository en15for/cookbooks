#
# Cookbook Name:: cookbook-r
# Recipe:: rstudio_packages
#
# Copyright (c) 2015 The Authors, All Rights Reserved.
#

# install Rcpp (dependencie for many of the other packages)
r_package "Rcpp"

# install system dependencies for xml type packages
case node["platform_family"]
when "debian"
  package "libxml2-dev" do
    action :install
  end
when "rhel"
  package "libxml2-devel" do
    action :install
  end
end

# install system dependencies for RCurl and other related packages
case node["platform_family"]
when "debian"
  package "libcurl4-openssl-dev" do
    action :install
  end
when "rhel"
  package "libcurl-devel" do
    action :install
  end
end

# install system dependencies for git2r package

case node["platform_family"]
when "debian"
  package ["libssl-dev", "libssh2-1-dev"] do
    action :install
  end
when "rhel"
  package ["openssl-devel", "libssh2-devel"] do
    action :install
  end
end


r_package "knitr"
r_package "rmarkdown"
r_package "RJSONIO"
r_package "shiny"
r_package "htmlwidgets"
r_package "devtools"
r_package "packrat"
r_package "testthat"


