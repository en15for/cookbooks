#
# Cookbook Name:: cookbook-r
# Recipe:: shiny_autotest
#
# Copyright (c) 2015 The Authors, All Rights Reserved.
#
# Purpose: Install system utilities and R packages to support automated Shiny app testing

# download selenium server standalone and place it in /usr/local/bin
selenium_version = "2.47.1"
selenium_file = "selenium-server-standalone-#{selenium_version}.jar"
#selenium_url = "ftp://selenium-release.storage.googleapis.com/index.html?path=2.47/#{selenium_file}"
selenium_url = "http://storage.googleapis.com/selenium-release/2.47/#{selenium_file}"

remote_file "/usr/local/bin/#{selenium_file}" do
  source selenium_url
  mode '0755'
  not_if "test -f /usr/local/bin/#{selenium_file}"
end

r_package "RSelenium"
r_dev_package "johndharrison/rDVR"

