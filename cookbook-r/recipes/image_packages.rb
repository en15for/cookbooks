#
# Cookbook Name:: cookbook-r
# Recipe:: image_packages
#
# Copyright (c) 2015 The Authors, All Rights Reserved.
#


# install system dependencies for png package
case node["platform_family"]
when "debian"
  package "libpng-dev" do
    action :install
  end
when "rhel"
  package "libpng-devel" do
    action :install
  end
end

# install system dependencies for jpeg package
case node["platform_family"]
when "debian"
  package "libjpeg-dev" do
    action :install
  end
when "rhel"
  package "libjpeg-turbo-devel" do
    action :install
  end
end


# install system dependencies for poppler
case node["platform_family"]
when "debian"
  package ['poppler-utils',  'poppler-data',  'libpoppler-dev', 'libpoppler-glib-dev'] do
    action :install
  end
when "rhel"
  package ['poppler',  'poppler-devel',  'poppler-glib'] do
    action :install
  end
end

# install system dependencies for cairo libraries
case node["platform_family"]
when "debian"
  package ["libcairo2-dev"] do
    action :install
  end
when "rhel"
  package ["cairo-devel"] do
    action :install
  end
end

# install system dependencies for ghostscript libraries
package ["ghostscript"] do
  action :install
end

# install system dependencies for spectre libraries
case node["platform_family"]
when "debian"
  package ["libspectre-dev"] do
    action :install
  end
when "rhel"
  package ["libspectre"] do
    action :install
  end
end

# install system dependencies for rsvg libraries
case node["platform_family"]
when "debian"
  package ["librsvg2-2"] do
    action :install
  end
when "rhel"
  package ["librsvg2-devel"] do
    action :install
  end
end

# install graphviz
package ["graphviz"] 

# install inkscape
package ["inkscape"] 

# install imagemagick
case node["platform_family"]
when "debian"
  package ["imagemagick"] do
    action :install
  end
when "rhel"
  package ["ImageMagick"] do
    action :install
  end
end

# install system dependencies for V8 libraries
case node["platform_family"]
when "debian"
  package ["v8-dev"] do
    action :install
  end
when "rhel"
  package ["v8-devel"] do
    action :install
  end
end

r_package "png"
r_package "svgPanZoom"
r_package "gridSVG"
r_dev_package "sjp/grConvert"

