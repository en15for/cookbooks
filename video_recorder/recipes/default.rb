#
# Cookbook Name:: video_recorder
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

prefix_dir = node['video-recorder-service']['prefix_dir']

include_recipe "build-essential"
include_recipe "yum-epel" if node['platform_family'] == 'rhel'
include_recipe "apt" if node['platform_family'] == 'debian'
include_recipe "maven"

# add something here

node['video-recorder-service']['src_deps'].each do |pkg|
  package pkg do
    action :install
  end
end

# download monte medial library and extract archive
remote_file "#{Chef::Config['file_cache_path']}/#{node['monte-media']['file']}" do
  source node['monte-media']['url']
  mode '0644'
  not_if "test -f #{Chef::Config['file_cache_path']}/#{node['monte-media']['file']}"
end

# build and install the monte media library
bash 'install monte-media lib' do
  cwd Chef::Config['file_cache_path']
  code <<-EOF
  unzip #{node['monte-media']['file']} && \
  mvn install:install-file -Dfile=MonteMediaCC/dist/monte-cc.jar -DgroupId=org.monte -DartifactId=screen-recorder -Dversion=0.7.7 -Dpackaging=jar
  EOF
  not_if "test -f #{Chef::Config['file_cache_path']}/MonteMediaCC/build-montemediacc.xml"
end

# clone VideoRecorderService repo from forked version of John Harrison
execute "clone VideoRecorderService Git repo" do
  cwd Chef::Config['file_cache_path']
  command <<-COMMAND
  git clone git://github.com/johndharrison/VideoRecorderService.git
  COMMAND
  not_if "test -f VideoRecorderService/pom.xml"
 end

# build videorecorderservice
execute "build VideoRecorderService" do
  cwd Chef::Config['file_cache_path']
  command <<-COMMAND
  cd VideoRecorderService && mvn package
  cp #{Chef::Config['file_cache_path']}/VideoRecorderService/target/videorecorderservice-2.0.jar #{prefix_dir}
  COMMAND
  not_if "test -f #{prefix_dir}/videorecorderservice-2.0.jar"
end

