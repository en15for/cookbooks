default['monte-media']['version'] = "0.7.7"
default['monte-media']['file'] = "MonteMedia-#{node['monte-media']['version']}src-cc.zip"
default['monte-media']['url'] = "http://www.randelshofer.ch/monte/files/#{node['monte-media']['file']}"

default['video-recorder-service']['prefix_dir'] = "/usr/local/bin"
default['video-recorder-service']['src_deps'] = %w{ git }
default['video-recorder-service']['build_options'] = %W{-Dfile=#{Chef::Config['file_cache_path'] }}
