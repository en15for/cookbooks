#
# Cookbook Name:: xfce
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

# add epel repo
yum_repository 'epel' do
  description 'Extra Packages for Enterprise Linux'
  mirrorlist 'http://mirrors.fedoraproject.org/mirrorlist?repo=epel-6&arch=$basearch'
  gpgkey 'http://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6'
  action :create
end

reboot 'xfce install reboot' do
  action :nothing
end

bash "yum groupinstall XFCE desktop" do
  user "root"
  group "root"
  code <<-EOC
    yum groupinstall "Xfce" -y
  EOC
  #not_if "yum grouplist installed | grep 'Xfce'"
  not_if "rpm -qa | grep xfce"
  notifies :request_reboot, 'reboot[xfce install reboot]', :delayed
end

