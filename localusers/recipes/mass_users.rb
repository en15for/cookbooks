#
# Cookbook Name:: localusers
# Recipe:: mass_users
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
# 
# Recipe to add a large group of users to a server (for use in an online course)

users_manage "testusers" do
  group_id 1138
  action [ :create]
end

users_manage "rstudio_users" do
  group_id 1139
  action [ :create]
end

