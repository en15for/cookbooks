name             'rstudio-server'
maintainer       'Eric Nantz'
maintainer_email 'thercast@gmail.com'
license          'GPL-2'
description      'Installs/Configures rstudio-server'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "apt"
depends "nginx"
depends "r"
depends "gdebi"
