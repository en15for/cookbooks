name             'ffmpeg_local'
maintainer       'YOUR_COMPANY_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures ffmpeg_local'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

supports 'redhat'
supports 'centos'
depends "yum-epel"
depends "yum"
