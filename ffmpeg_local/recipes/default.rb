#
# Cookbook Name:: ffmpeg_local
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'yum-epel'

# define necessary pre-requisite packages to be installed by yum
pkgs = %w{ gcc-c++ make yasm pkgconfig libXext-devel libXfixes-devel zlib-devel }

pkgs.each do |pkg|
  package pkg
end

# download release source archive from ffmpeg site
remote_file "#{Chef::Config['file_cache_path']}/ffmpeg-#{node['ffmpeg']['version']}.tar.bz2" do
  source node['ffmpeg']['url']
  mode '0644'
  not_if "test -f #{Chef::Config['file_cache_path']}/ffmpeg-#{node['ffmpeg']['version']}.tar.bz2"
end

# execute build
execute "Extracting and building ffmpeg #{node['ffmpeg']['version']} from source" do
  not_if "test -f /usr/local/bin/ffmpeg"
  cwd Chef::Config['file_cache_path']
  command <<-COMMAND
    (mkdir ffmpeg-#{node['ffmpeg']['version']})
    (tar xfj ffmpeg-#{node['ffmpeg']['version']}.tar.bz2 --strip-components 1 -C ffmpeg-#{node['ffmpeg']['version']})
    (cd ffmpeg-#{node['ffmpeg']['version']} && ./configure #{node['ffmpeg']['config_opts']} && make prefix=#{node['ffmpeg']['prefix']} install)
  COMMAND
  #not_if "ffmpeg --version | grep #{node['ffmpeg']['version']}"
  #not_if (File.exists?("/usr/local/bin/ffmpeg"))
end

# post-install configuration
# move ld.so.conf template file to /etc
# then run command ldconfig

cookbook_file "/etc/ld.so.conf" do
  source "ld.so.conf"
  mode "0644"
end

execute "ldconfig" do
  only_if do
    File.exists?("/etc/ld.so.conf")
  end
end
 
